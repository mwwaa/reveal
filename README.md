# Reveal

A simple CloudFlare worker to avoid content scraping by robots, using Turnstile captcha.

## Setup

A Makefile is used to simplify setup/as a wrangler commands memo. It's using podman for sandboxing wrangler.

Prerequisites:

* [Podman](https://podman.io)
* Get a CloudFlare API key and set it as the value of the `CLOUDFLARE_API_TOKEN` environment variable.
* Setup a CloudFlare Turnstile captcha and get the site key and secret.

1. `make build`
2. `make create-kv-ns` and copy/paste the `{ binding = "reveal", id = "..."}` in the appropriate place in `wrangler.toml`
3. List your CORS origins in the specified `[vars]` in `wrangler.toml`
4. `make set-captcha-secret` and paste your Turnstile secret key when asked (obtained from CloudFlare).
5. `make ID=key VALUE=secret set-revelation` with key being a identifier for your protected content and secret as the protected content.
6. `make deploy`
7. Get the deployed worker's URL
8. See `example/reveal.html` for how to use it on your page.
