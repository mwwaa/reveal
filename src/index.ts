export interface Env {
    reveal: KVNamespace;
}

export default {
    async fetch(
        request: Request,
        env: Env,
        ctx: ExecutionContext
    ): Promise<Response> {
        const origin = request.headers.get("Origin");
        const allowedOrigins = env.ALLOW_ORIGINS.split(",");
        let corsHeaders = {
            "Access-Control-Allow-Origin": allowedOrigins[0],
            "Access-Control-Allow-Methods": "POST,OPTIONS"
        };
        if (allowedOrigins.includes(origin)) {
            corsHeaders = {
                "Access-Control-Allow-Origin": origin,
                "Access-Control-Allow-Methods": "POST,OPTIONS"
            };
        }

        if (request.method === "OPTIONS") {
            return new Response(null, {headers: corsHeaders});
        }

        if (request.method !== "POST") {
            return new Response(null, {status: 405});
        }

        const body = await request.json();
        const r = body.r; // revelation
        const cr = body.cr; // captcha response
        const ip = request.headers.get('CF-Connecting-IP');

        // Validate the token by calling the
        // "/siteverify" API endpoint.
        let formData = new FormData();
        formData.append('secret', env.CAPTCHA_SECRET_KEY);
        formData.append('response', cr);
        formData.append('remoteip', ip);

        const url = 'https://challenges.cloudflare.com/turnstile/v0/siteverify';
        const result = await fetch(url, {
            body: formData,
            method: 'POST',
        });

        const outcome = await result.json();
        if (outcome.success) {
            const value = await env.reveal.get(r);
            if (value === null) {
                return new Response(null, {status: 404});
            }
            return new Response(value, {headers: corsHeaders});
        }
        return new Response(null, {status: 400});
    },
};
