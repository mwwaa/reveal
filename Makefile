# Helps run wrangler in a container instead of locally, using podman.
default: build run
.PHONY: build run assert_api_token

build:
	podman build . -t reveal

run:
	podman run -it --rm --env CLOUDFLARE_API_TOKEN=$${CLOUDFLARE_API_TOKEN} -p 8787:8787 reveal

create-kv-ns:
	podman run -it --rm --env CLOUDFLARE_API_TOKEN=$${CLOUDFLARE_API_TOKEN} reveal kv:namespace create reveal

set-captcha-secret:
	podman run -it --rm --env CLOUDFLARE_API_TOKEN=$${CLOUDFLARE_API_TOKEN} reveal secret put CAPTCHA_SECRET_KEY

set-revelation:
	podman run -it --rm --env CLOUDFLARE_API_TOKEN=$${CLOUDFLARE_API_TOKEN} reveal kv:key put --preview false --binding reveal ${ID} ${VALUE}

deploy: build
	podman run -it --rm --env CLOUDFLARE_API_TOKEN=$${CLOUDFLARE_API_TOKEN} reveal publish
