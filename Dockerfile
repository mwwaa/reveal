FROM node:alpine

WORKDIR /reveal
COPY ./package.json ./package.json

RUN npm install

COPY . .

ENTRYPOINT ["npx", "wrangler"]
CMD ["dev"]
